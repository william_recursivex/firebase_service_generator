import 'package:analyzer/dart/element/element.dart';
import 'package:source_gen/source_gen.dart';
import 'package:build/build.dart';

import 'firebase_service.dart';

class FirebaseServiceGenerator extends GeneratorForAnnotation<FirebaseService> {
  @override
  generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) {
    String className = element.displayName;
//    String path = buildStep.inputId.path;
    String name = annotation.peek('name').stringValue;
    String col = annotation.peek('col').stringValue;

    String pageContent = '''import 'package:cloud_firestore/cloud_firestore.dart';\n''';
    pageContent += '''import 'package:flutter/material.dart';\n''';
    pageContent += 'import \'$name.dart\';\n\n';
    pageContent += 'class ${className}FirebaseService {\n';
    pageContent += '''CollectionReference colRef = Firestore.instance.collection('$col');\n\n''';

    pageContent += '''
    static final ${className}FirebaseService _singleton = ${className}FirebaseService._internal();

    factory ${className}FirebaseService() {
      return _singleton;
    }
  
    ${className}FirebaseService._internal();
    
    ''';

    // find
    pageContent += '''
    Stream<List<$className>> find({Query query, dynamic orderField, bool descending = false, int limit}) {
      Query inColRef = colRef;
      if (query != null) {
        inColRef = query;
      }
      if (orderField != null) {
        inColRef = inColRef.orderBy(orderField, descending: descending);
      }
      if (limit != null) {
        inColRef = inColRef.limit(limit);
      }
      return inColRef.snapshots().map((list) {
        return list.documents.map((doc) {
          final Map<String, dynamic> data = doc.data;
          data['id'] = doc.documentID;
          return $className.fromJson(data);
        }).toList();
      });
    }
    
    Stream<$className> findOne({Query query, dynamic orderField, bool descending = false}) {
      Query inColRef = colRef;
      if (query != null) {
        inColRef = query;
      }
      if (orderField != null) {
        inColRef = inColRef.orderBy(orderField, descending: descending);
      }
      return inColRef.limit(1).snapshots().map((list) {
        return list.documents.map((doc) {
          final Map<String, dynamic> data = doc.data;
          data['id'] = doc.documentID;
          return $className.fromJson(data);
        }).first;
      });
    }
  
    Stream<$className> findById({@required String id}) {
      if (id == null) {
        return null;
      }
      return colRef.document(id).snapshots().map((doc) {
        final Map<String, dynamic> data = doc.data;
        data['id'] = doc.documentID;
        return $className.fromJson(data);
      });
    }
    ''';

    // Insert & update
    pageContent += '''
    Future<DocumentReference> insert({@required Map<String, dynamic> data}){
      return colRef.add(data);
    }
  
    Future<void> update({@required String id, @required Map<String, dynamic> data}){
      return colRef.document(id).updateData(data);
    }
    ''';

    pageContent += '''
    }
    
    ${className}FirebaseService ${name}FirebaseService = ${className}FirebaseService();
    ''';

    return pageContent;
  }
}