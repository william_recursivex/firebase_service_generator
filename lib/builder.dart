import 'package:source_gen/source_gen.dart';
import 'package:build/build.dart';
import 'firebase_service_generator.dart';

Builder firebaseServiceBuilder(BuilderOptions options) => LibraryBuilder(FirebaseServiceGenerator(),
  generatedExtension: '.service.dart',
);